# Your paper and video summaries go here.

# Rubric:
* Write a short summary of the paper/video, as follows:
* First line: Paper Title, citation source
* Keywords: List keywords from paper, or select 4-8 most relevant terms from the paper
* First paragraph: describe the main points of the paper/video.
* Second paragraph: Present the paper/video's strengths and weaknesses from your point of view.

* PSP Script Version2 (Personal Software Process for Engineers)
* Keywords: Planning, Development, Acceptance Test, Evaluation 
* This article presents the personal software process for engineers to help them develop module-level programs. The process consists of entry criteria, planning, development, acceptance testing, evaluation, and exit criteria. During the planning stage, it is important to have clear and specific problem and requirement statements. The author recommends estimating the number of lines of code and time required for each component and providing both individual and total estimates. In the evaluation stage, the author suggests documenting the time spent on the software, significant problems, and areas for improvement. 
* One strength of this article is its clear and concise organization, which uses tables to illustrate each stage of the personal software process. This makes it easy to understand for those who are new to programming. However, a possible weakness is that the article lacks an example of when to use the PSP in practical situations.

* Are We There Yet? Agile Situational Awareness Using Estimation by George Dinwiddie at DCSUG.
* Keywords: Estimation, Plan, Situation Awareness, Time, Assumptions, Agile, Adjust
* In this video, George Dinwiddie discusses the difference between estimates and plans, using a real-world analogy of travel time to illustrate his points. He emphasizes that estimates can help identify possible issues and make informed decisions, while plans require more specific and detailed information. Dinwiddie also stresses the importance of trust and communication in agile initiatives. He walks through an example of how to calculate travel time for a conference, including how to record assumptions and recognize inaccuracies in estimates. He concludes by emphasizing the importance of being prepared for the unexpected and using estimates as a tool for identifying false assumptions. Overall, the video provides practical advice for effective estimating and planning in agile projects.
* One strength of this video is its use of a relatable real-life example to illustrate the difference between estimates and plans. It is also presented in a clear and approachable manner, making it accessible to those without extensive technical knowledge. Additionally, the layout of the PowerPoint is well-designed and visually appealing without being overly distracting. However, a possible weakness is that the viewer is not privy to the discussions that take place in the breakout room, which could provide additional insights and perspectives on the topic. It would be more comprehensive if the video included recordings of the breakout room discussions.

