How many customers are in the database?

query: SELECT COUNT(*) FROM customers
answer: 59


As measured by number of tracks sold, what is the most popular genre?


query: SELECT genres.Name,COUNT(trackid) FROM tracks
...> JOIN genres
...> ON genres.Genreid = tracks.Genreid
...> GROUP BY genres.Name
...> ORDER BY COUNT(trackid) DESC;
...> LIMIT 1;
answer: Rock (Total number: 1297)


As measured by number of tracks sold, what is the least popular genre?


query: SELECT genres.Name,COUNT(trackid) FROM tracks
...> JOIN genres
...> ON genres.Genreid = tracks.Genreid
...> GROUP BY genres.Name
...> ORDER BY COUNT(trackid);
answer: Opera (Total number sold: 1)


Who is the most popular artist, as measured by number of tracks sold?


query: SELECT artists.Name,COUNT(trackid) FROM tracks
...> JOIN albums
...> ON tracks.Albumid = albums.Albumid
...> JOIN artists
...> ON albums.Artistid = artists.Artistid
...> GROUP BY artists.Name
...> ORDER BY COUNT(trackid) DESC;
answer: Iron Maiden (Total number: 213)


How many albums by 'Miles Davis' are in the database?


query:
SELECT COUNT(AlbumId) FROM albums
...> JOIN artists
...> ON albums.ArtistId=artists.ArtistId
...> WHERE Name='Miles Davis’;
answer: 3



What is the name of the longest song in the database?
query: SELECT name, Milliseconds FROM tracks
...> ORDER BY Milliseconds DESC
...> LIMIT 1;
answer: Homecoming / The Death Of St. Jimmy / East 12th St./ Nobody Likes You / Rock And Roll Girlfriend / We're Coming Home Again


What is the title and length (in milliseconds) of the longest album in the database?



Query: SELECT albums.Title, SUM(Milliseconds) FROM tracks
...> JOIN albums
...> ON tracks.Albumid = albums.Albumid
...> GROUP BY albums.Title
...> ORDER BY SUM(Milliseconds) DESC
...> LIMIT 1;
answer: Title: Lost, Season 3, Length: 70665582


What is the invoice id, amount, and customer name of the most expensive invoice?


query:
SELECT invoices.Invoiceid, SUM(invoice_items.UnitPriceinvoice_items.Quantity), customers.FirstName, customers.Lastname
...> FROM invoice_items
...> JOIN invoices
...> ON invoices.Invoiceid = invoice_items.Invoiceid
...> JOIN customers
...> ON invoices.Customerid = customers.Customerid
...> GROUP BY invoices.Invoiceid
...> ORDER BY SUM(invoice_items.UnitPriceinvoice_items.Quantity) DESC
...> LIMIT 1;
answer: Invoice id: 404, Amount: 25.86, Customer name: Helena Holy


What is the name of the customer who has spent the most?


query: SELECT SUM(invoice_items.UnitPriceinvoice_items.Quantity), customers.FirstName, customers.Lastname
...> FROM invoice_items
...> JOIN invoices
...> ON invoices.Invoiceid = invoice_items.Invoiceid
...> JOIN customers
...> ON invoices.Customerid = customers.Customerid
...> GROUP BY customers.Customerid
...> ORDER BY SUM(invoice_items.UnitPriceinvoice_items.Quantity) DESC
...> LIMIT 1;
answer: Helena Holy (Spent: 49.62)