# Your paper and video summaries go here.

# Rubric:
* Write a short summary of the paper/video, as follows:
* First line: Paper Title, citation source
* Keywords: List keywords from paper, or select 4-8 most relevant terms from the paper
* First paragraph: describe the main points of the paper/video.
* Second paragraph: Present the paper/video's strengths and weaknesses from your point of view.

* Javascript Fundamentals https://info340.github.io/javascript.html
* Running javaScript, writing Scripts, Variables, Control Structures, Functions
* Basically, the article tells what javascript is, and some basic syntax.
* Clear syntax logic, but don't know how to install and use。

* Chapter 1Refactoring: A First Example https://www.oreilly.com/library/view/refactoring-improving-the/9780134757681/ch01.xhtml#ch01lev1sec1 
* Refactoring, decomposing the statement fct, example，Moving Functions into the Calculator，Reorganizing the Calculations by Type
* The author uses a code that coordinates performance information to show what it means to refactoring. And a step by step break down to explain how these codes should be adjusted.
* Logical, informative and easy to understand at a glance. But the article is a bit long.

* Automated testing with Mocha https://javascript.info/testing-mocha
* Behavior Driven Development, development of 'pow':the spec, the development flow, mocha, chai, sinon
* The author talks about the importance of testing, and how to go about it. And an example is made with a simple mathematical code.
* Easy to understand 

