var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
  }

  ready(() => {
      /* Do things after DOM has fully loaded */
      document.getElementById("Submit-button").addEventListener('click',handleClick);
      document.getElementById("stock-select").addEventListener('change',makeplot);
  });

function handleClick(event){
    console.log(event);
    bootbox.confirm("Do you want to submit the form?", function(result) {
        if (result) {
          alert("Form submitted.");
        } else {
          alert("Action cancelled.");
        }
    });
};

function makeplot() {
    console.log("makeplot: start")
    var select = document.getElementById("stock-select");
    var stock = select.value;
    fetch(stock +".csv")
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error)
         })
    .then((text) => {
      console.log("csv: start")
      csv().fromString(text).then(processData)  /* asynchronous */
      console.log("csv: end")
    })
    console.log("makeplot: end")

};


function processData(data) {
  console.log("processData: start")
  let x = [], y = []

  for (let i=0; i<data.length; i++) {
      row = data[i];
      x.push( row['Date'] );
      y.push( row['Close'] );
  }
  makePlotly( x, y );
  console.log("processData: end")
}

function makePlotly( x, y ){
  console.log("makePlotly: start")
  var traces = [{
        x: x,
        y: y
  }];
  var layout  = { title: $("#stock-select option:selected").text() + " Stock Price History"}

  myDiv = document.getElementById('myDiv');
  Plotly.newPlot( myDiv, traces, layout );
  console.log("makePlotly: end")
};
