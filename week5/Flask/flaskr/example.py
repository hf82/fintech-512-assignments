from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
import json
import math
from .db import get_db

bp = Blueprint('example', __name__)

@bp.route('/statement', methods=('GET','POST'))
def statement():
    if request.method == "POST":
        invoice = json.loads(request.form['invoice'])
        plays = json.loads(request.form['plays'])
        print(invoice)
        print(type(invoice))
        print(plays)
        print(type(plays))
        total_amount = 0
        volume_credits = 0
        result = f'<h1>Statement for {invoice["customer"]}</h1>\n'
        result += f'<table>\n'
        result += f'<tr><th>play</th><th>seats</th><th>cost</th></tr>'

        def format_as_dollars(amount):
            return f"${amount:0,.2f}"

        for perf in invoice['performances']:
            play = plays[perf['playID']]
            if play['type'] == "tragedy":
                this_amount = 40000
                if perf['audience'] > 30:
                    this_amount += 1000 * (perf['audience'] - 30)
            elif play['type'] == "comedy":
                this_amount = 30000
                if perf['audience'] > 20:
                    this_amount += 10000 + 500 * (perf['audience'] - 20)

                this_amount += 300 * perf['audience']

            else:
                raise ValueError(f'unknown type: {play["type"]}')

            # add volume credits
            volume_credits += max(perf['audience'] - 30, 0)
            # add extra credit for every ten comedy attendees
            if "comedy" == play["type"]:
                volume_credits += math.floor(perf['audience'] / 5)
            # print line for this order
            result += f' <tr><td>{play["name"]}</td><td>{format_as_dollars(this_amount/100)}</td><td>({perf["audience"]} seats)</td></tr>\n'
            total_amount += this_amount

        result += f'<table>\n'
        result += f'<p>Amount owed is <em>{format_as_dollars(total_amount/100)}</em></p>\n'
        result += f'<p>You earned <em>{volume_credits}</em> credits</p>\n'
        return render_template('example/statement.html',result=result)

    return render_template('example/statement.html',result='')

@bp.route('/stockchart')
def stockchart():
    return render_template('example/index.html')



