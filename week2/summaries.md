# Your paper and video summaries go here.

# Rubric:
* Write a short summary of the paper/video, as follows:
* First line: Paper Title, citation source
* Keywords: List keywords from paper, or select 4-8 most relevant terms from the paper
* First paragraph: describe the main points of the paper/video.
* Second paragraph: Present the paper/video's strengths and weaknesses from your point of view.

# What Are Requirements?
* https://learning.oreilly.com/library/view/mastering-the-requirements/0321419499/ch01.html#ch01
* Requirements, user's purpose, sign-on requirements, constraints 
* This paper begins by explaining what a requirement is and why we are interested in it. It states that requirements should be expressed in a technology-neutral way and not frozen at build time. The paper then introduces agile software development and its four manifestos. The paper also mentions agile ratings. There are three agile ratings - Rabbit, Horse, and Elephant.
* This article precisely indicates the purpose. But it has a complex structure, which probably mislead readers. 

# A Laboratory For Teaching Object-Oriented Thinking
* https://sakai.duke.edu/access/content/group/FINTECH-512-02-Sp23/Papers/Beck%20-%20%20A%20Laboratory%20For%20Teaching%20Object-Oriented%20Thinking.pdf
* Problem, Perspective,CRC Cards, Efficiency 
* This paper introduces what is CRC cards, and why it's useful. CRC cards can directly categorize the object's properties, which can help leaner to understand the target more quickly, and also can help people to make a summary based on its characters. 
* By discussing on the transformation of utilization to CRC cards, reader can get a big picture of it. 

# HTML Fundamentals
* https://info340.github.io/html-fundamentals.html 
* HTML Elements, Comments, Attributes, Empty Element, Structure
* This article mainly introduce the language of Coding HTML. 
* It's like a user instruction. 



