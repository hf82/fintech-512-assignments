# Your paper and video summaries go here.

# Rubric:
* Write a short summary of the paper/video, as follows:
* First line: Paper Title, citation source
* Keywords: List keywords from paper, or select 4-8 most relevant terms from the paper
* First paragraph: describe the main points of the paper/video.
* Second paragraph: Present the paper/video's strengths and weaknesses from your point of view.

* On the Criteria To Be Used in Decomposing Systems into Modules: https://sakai.duke.edu/access/content/group/FINTECH-512-02-Sp22/Papers/On%20the%20Criteria%20To%20Be%20Used%20in%20Decomposing%20Systems%20into%20Modules%20Parnas.pdf
* Keywords: software, modules, modularity, software engineering, KWIC index, software design 
* This article firstly discuss about advantages of using modules to write code. Moreover, author using examples to simply explain how to use module to write code. Then, this article compares two modules's changeability, independent development, and comprehensibility. Finally, author summarize this article that it is incorrect to decompose firstly based on flowchart. 
* This article state module intercept clearly and precisely, and provide a brand new way to implement code. By using two examples and making a comparison enables reader to understand more deeply. 

* Summarization of <On the Criteria To Be Used in Decomposing Systems into Modules>: https://blog.acolyer.org/2016/09/05/on-the-criteria-to-be-used-in-decomposing-systems-into-modules/
* Keywords: Agility, effectiveness, modularization, decomposition 
* Authors mainly summarize two examples, and make a comparison between two way of decomposition. 
* This article is more clearly than the original article. I have an precise understanding of those two modules. 

* 99 Bottles Sample JS: https://sandimetz.com/99bottles-sample-js#chapter-rediscovering-simplicity
* Keywords: Object-Oriented Design, simplification
* By using 99 bottles as OOD, author lists four way of decomposition which are incomprehensibly concise, speculatively general, concretely abstract, and shameless green. Based on each module, author analysis its advantages and flaws. We can simply see that efficiency and comprehensibility is negative correlation. 
* This article has simplicity, which lets reader to understand very easily.

* GORUCO 2009: https://www.youtube.com/watch?v=v-2yFMzxqwU
* Keywords: Achieving independence,SOLID principle, dependency interjection, Ground Rules. 
* The video begins by describing how, although the application starts out perfectly, if things change, the dependencies can "die". A way to use design to save the day. The speaker then mentions five principles of object-oriented design, namely the single responsibility principle, the open closure principle, the Liskov substitution principle, the interface isolation principle, and the dependency inversion principle, which enable independence. To avoid dependencies, the code should be loosely coupled, highly cohesive, easy to assemble, and context independent. The speaker also mentioned that if testing is difficult, that means you should change the design of the code. She then used an example to mention more about the open-closed principle, the basic rules, the resistance to resources, the way to mock the reasons for refactoring, the way to build a configuration class that is unlikely to change. Finally, she shows that Test Driven Development (TDD) and "Don't Repeat Yourself" (DRY) are not enough and that following these principles will keep the code flexible, easy to change and fun to work with.
* The presentation was very interesting and engaged the audience; the speaker mentioned the 5 principles of object-oriented design, which were easy to understand.




