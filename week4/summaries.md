# Your paper and video summaries go here.

# Rubric:
* Write a short summary of the paper/video, as follows:
* First line: Paper Title, citation source
* Keywords: List keywords from paper, or select 4-8 most relevant terms from the paper
* First paragraph: describe the main points of the paper/video.
* Second paragraph: Present the paper/video's strengths and weaknesses from your point of view.


The SOLID Design Principles Deconstructed
https://www.youtube.com/watch?v=tMW08JkFrBA
Key words: SOLID design principles, software development, code maintenance, Scalability, best practices, real-world examples, practical insights
The speaker deconstructs the SOLID principles, explaining each one in detail and offering insights into how they can be applied in practice. Specifically, the SOLID principles include: Single Responsibility Principle (SRP) - A class should have only one reason to change, meaning it should have only one responsibility. Open-Closed Principle (OCP) - Software entities should be open for extension but closed for modification. Liskov Substitution Principle (LSP) - Subtypes should be substitutable for their base types. Interface Segregation Principle (ISP) - Clients should not be forced to depend on interfaces they do not use. Dependency Inversion Principle (DIP) - High-level modules should not depend on low-level modules. Both should depend on abstractions. Henney provides real-world examples to help illustrate the concepts and provide a deeper understanding of how the principles can be applied in practice. He also offers insights into common challenges faced when implementing these principles and provides tips for overcoming them.
The strengths of the article include:
clear Explanation: Kevlin Henney provides clear and concise explanations of the SOLID design principles, making the concepts easy to understand for developers at all levels.
Practical Examples: The video includes real-world examples that help to illustrate the concepts behind the principles and how they can be applied in practice.
Besides, the weakness include:
Limited Depth: While the video provides a solid overview of the SOLID principles, it may not provide the level of detail and in-depth analysis required by advanced developers.
Limited Interactivity: As a video presentation, it does not offer opportunities for hands-on practice or interaction with the material.
