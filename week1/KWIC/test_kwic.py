import unittest

from kwic import KWIC

class Testkwic(unittest.TestCase):


    def test_norm(self):
        k = KWIC('input1.txt')
        x = k.writeout()
        stdanswer = []
        f = open('output.txt', 'r')
        for line in f:
            line = line.strip()
            stdanswer.append(line)
        self.assertEqual(x, stdanswer)

if __name__ == '__main__':
    unittest.main()
