import sys


class WordsToIgnore:
    total_words = 0
    total_chars = 0
    ignoreWords = []

    def __init__(self, word) -> None:
        self.word = word
        self.char = len(word)

        if self.char > 10:
            raise ValueError('Words above :: cannot be more than 10 characters.')

        if not self.word.islower():
            raise ValueError('Words above :: need to be lower case.')

        if not self.word.isalpha():
            raise ValueError('For words above ::, one word per line, a-to-z characters only. ')

        # Tested next two functions
        WordsToIgnore.total_words += 1
        if WordsToIgnore.total_words > 50:
            raise ValueError('Cannot have more than 50 words.')

        WordsToIgnore.total_chars += self.char  # don't need to check for 10,000 characters until titles; will use later

    def ignore_list_details(self):
        print(self.word)
        print(self.char)
        print(self.total_words)
        print(self.total_chars)
        print(self.ignoreWords)

    def append_list(self):
        self.ignoreWords.append(self.word)


class Title:
    total_titles = 0
    total_chars = 0
    titles = []

    def __init__(self, title) -> None:
        self.title = title
        self.char = len(title)
        self.words_in_title = len(self.title.split())

        if self.words_in_title > 15:
            raise ValueError('Title are listed by line on input file and cannot be longer than 15 words')

        Title.total_titles += 1
        if Title.total_titles > 200:
            raise ValueError('Cannot have more than 200 titles.')

        Title.total_chars += self.char  # total of 10000 chars on all words on list allowed
        if Title.total_chars + WordsToIgnore.total_chars > 10000:
            raise ValueError('Input file cannot have more than 10000 characters')

        for c in self.title:  # to check all characters to be either space or a-z
            if not c.isalpha():
                if not c.isspace():
                    raise ValueError('Only titles with A-Z chars (upper/lowercase) allowed; whitespace ok')

    def title_list_details(self):
        print(self.title)
        print(self.char)
        print(self.words_in_title)
        print(self.total_titles)
        print(self.total_chars)
        print(self.titles)

    def append_list(self):
        self.titles.append(self.title)


# 'input1.txt'
# This section is to (1) pull the input file and (2) build the WordsToIgnore and Titles lists
print('Please provide a file that (1) lists words to ignore on the top; (2) titles on the bottom; (3) both lists '
      'separated by ::')
print('Each entry has its own line. Words on top are only a-z characters that are less than 10 characters.')
print('Titles have A-z characters and less than 15 words. Whitespace allowed')
print('Please enter your file name. For a demo, enter input1.txt:\n')
filename = sys.stdin.readline().strip()
with open(filename, 'r') as f:
    switch = 0
    for line in f:
        if line == '::\n':
            switch = 1
            continue
        if switch == 0:
            a = WordsToIgnore(line.strip())
            a.append_list()
            # a.ignore_list_details()  # to print variables for easy visibility in testing
        if switch == 1:
            b = Title(line.strip())
            b.append_list()
            # b.title_list_details() # to print variables for easy visibility in testing
    f.close()

keywords = []
for i in range(len(Title.titles)):
    titleSplit = Title.titles[i].split()
    # titleSplit = titleSplit.lower()
    for w in range(len(titleSplit)):
        # print(titleSplit[w])
        if titleSplit[w].lower() in WordsToIgnore.ignoreWords:
            continue
        else:
            keywords.append(titleSplit[w].upper())

keywords = list(set(keywords))
keywords.sort()
output = []
# print(keywords)

# test output of program; need to put through stdout
KWIC_output = sys.stdout
for i in range(len(keywords)):
    # print(keywords[i])
    for j in range(len(Title.titles)):
        titleSplit = Title.titles[j].lower()
        titleSplit = titleSplit.split()
        # print(Title.titles[j])
        # print(titleSplit)
        for k in range(len(titleSplit)):
            # print(keywords[i])
            # print(titleSplit[k])
            if keywords[i] == titleSplit[k].upper():
                output = titleSplit
                output[k] = titleSplit[k].upper()
                # print(output)
                KWIC_output.write(' '.join(output) + '\n')
                # KWIC_output.write('\n')
                output[k] = output[k].lower()

                # Need a stdout formula here with some looping; read instructions and requirements before proceeding
