import os

class KWIC:
    def __init__(self,filename):
        list=[]
        notkey=[]
        text=[]

        with open(filename,'r') as f:
            for line in f:
                line = line.strip().lower()
                list.append(line)
                if line == "::":
                    x = list.index("::")

        notkey=list[0:x]
        text=list[x+1:]
        key=[]
        for str in text:
            temp = str.split(' ')
            for word in temp:
                if word not in notkey and word not in key:
                    key.append(word)
        print(key)
        key.sort()
        print(key)

        self.key = key
        self.text = text

    def writeout(self):
        text = []
        for key in self.key:
            for str in self.text:
                str = str.split(" ")
                num = str.count(key)
                if num ==0: continue
                pos = -1
                for i in range(0,num):
                    pos = str.index(key,pos+1)
                    temp = str.copy()
                    if pos >=0:
                        temp[pos]=key.upper()
                    else:
                        break
                    temp = " ".join(temp)
                    text.append(temp)

        return text



if __name__=='__main__':
    k = KWIC('input1.txt')
    text=k.writeout()
    with open("output.txt", "a") as f:
        for item in text:
            f.write("%s\n" %item)



