# Your paper and video summaries go here.

# Rubric:
* Write a short summary of the paper/video, as follows:
* First line: Paper Title, citation source
* Keywords: List keywords from paper, or select 4-8 most relevant terms from the paper
* First paragraph: describe the main points of the paper/video.
* Second paragraph: Present the paper/video's strengths and weaknesses from your point of view.

# No Silver bullet - Essence and accident in software engineering
* source: https://sakai.duke.edu/access/content/group/FINTECH-512-02-Sp22/Papers/Brooks-NoSilverBullet.pdf
* Keywords: Complexity, conformity, changeability, invisibility, high-level language, AI 
* In this article, author mainly discuss that the magic shortcut does not exit. Any solution has its pros and cons. 
* Strengths: introduce the shinning point of IT industry.

# Implementing VisiCalc 
* source: https://rmf.vc/ImplementingVisiCalc
* Keywords: Programming decision, calc and formula, Keyboard Usage and Interacting with VisiCalc 
* The main point can be "focus on problem" which is customer needs and wants. Moreover, author discuss about how to build a program step by step.
* Strengths: Details about developing software and utilizing tools.

# The History (and Future) of Software', Booch 
* source: https://www.youtube.com/watch?v=OdI7Ukf-Bf4
* Keywords: big picture, software 
* This is a video about software development. Developing software is like making a car, each part has its purpose. When combined together, a new thing is born, a tool that can help people.
* Strengths: Speaker simply explains the process and purpose of developing software.